#!/bin/bash -x

export PATH=$CONDA_DIR/bin:$PATH
# Install Jupyter notebook
conda install --yes 'notebook=5.0*' terminado
conda clean -yt

#Create Jupyter working folders
mkdir /root/work
mkdir /root/.jupyter
mkdir /root/.local
