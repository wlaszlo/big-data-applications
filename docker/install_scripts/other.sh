#!/bin/bash -x

export PYTHONPATH=$SPARK_HOME/python:$SPARK_HOME/python/lib/py4j-0.10.4-src.zip
export PATH=$CONDA_DIR/bin:$PATH

# Install Python packages
conda install --yes 'ipython' 'ipywidgets' 'pandas' 'matplotlib' 'scipy' 'seaborn' 'scikit-learn' 'pyspark' 'py4j' pyzmq
conda clean -yt

# Scala Spark and Pyspark kernels
mkdir -p /opt/conda/share/jupyter/kernels/scala
mkdir -p /opt/conda/share/jupyter/kernels/pyspark

cp /work/kernels/scala.json /opt/conda/share/jupyter/kernels/scala/kernel.json
cp /work/kernels/pyspark.json /opt/conda/share/jupyter/kernels/pyspark/kernel.json