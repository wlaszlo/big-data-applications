#!/bin/bash -x

# Spark dependencies
apt-get -y update
apt-get install -y --no-install-recommends openjdk-7-jre-headless
apt-get clean

echo 'Downloading Spark. Hold tight..'
wget -qO - http://d3kbcqa49mib13.cloudfront.net/spark-${APACHE_SPARK_VERSION}-bin-hadoop2.7.tgz | tar -xz -C /usr/local/
cd /usr/local
ln -s spark-${APACHE_SPARK_VERSION}-bin-hadoop2.7 spark