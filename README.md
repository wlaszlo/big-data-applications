# Docker-Spark2

This repo is the fork of [vagrant-spark2](https://github.com/igabriel85/vagrant-spark2.git) which is a fork of [vagrant-jupyter](https://github.com/giabar/vagrant-jupyter) which is based on Spark 1.5.1 and Jupyter v4.

This is a Docker image with Jupyter Notebook (v5) installed.
The kernels installed are: Python 3.7.4, Apache Spark 2.1.1 with Scala 2.11.8 and PySpark (2.1.1).

The notebook folder is: `/home/jupyter`, which is mounted on host as a Docker volume.


### Requirements
You have to install:

1. [Docker](https://docs.docker.com/v17.09/engine/installation/)
2. [Docker Compose](https://docs.docker.com/compose/install/)


### Run

```
git clone git@gitlab.com:wlaszlo/big-data-applications.git
cd big-data-applications/docker
docker-compose up -d
```

When the Docker pulls the image you can point your browser to:

```
http://localhost:8897
```


### Developer - rebuild docker image
```
docker build --network=host -t big-data-app:1.0 .
```


### If you're behind proxy:

Check [https://docs.docker.com/network/proxy/](https://docs.docker.com/network/proxy/)