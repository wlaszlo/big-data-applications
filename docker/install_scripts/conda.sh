#!/bin/bash -x

# Install conda
mkdir -p $CONDA_DIR
echo export PATH=$CONDA_DIR/bin:'$PATH' > /etc/profile.d/conda.sh

MINICONDA_SCRIPT=Miniconda3-latest-Linux-x86_64.sh

wget https://repo.continuum.io/miniconda/$MINICONDA_SCRIPT
chmod +x $MINICONDA_SCRIPT
/bin/bash $MINICONDA_SCRIPT -f -b -p $CONDA_DIR

rm $MINICONDA_SCRIPT
$CONDA_DIR/bin/conda config --append channels conda-forge
$CONDA_DIR/bin/conda install --yes conda==4.7.11
